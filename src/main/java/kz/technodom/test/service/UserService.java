package kz.technodom.test.service;

import kz.technodom.test.entity.Stat;
import kz.technodom.test.entity.User;
import kz.technodom.test.entity.UserStatusDto;
import kz.technodom.test.error.ResourceNotFoundException;
import kz.technodom.test.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserService {

    private UsersRepository usersRepository;

    @Autowired
    public UserService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findById(@PathVariable("id") long id) {
        User user = usersRepository.findById(id);
        if (user == null) {
            throw new ResourceNotFoundException("User not found");
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findAll(@RequestParam(name = "status", required = false) User.Status status, @RequestParam(name = "id", required = false) Long id) {
        List<User> users = usersRepository.findUsers(status);
        return new ResponseEntity<>(new Stat(id, users), HttpStatus.OK);
    }

    @RequestMapping(value = "create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody @Valid User user) {
        user.setCreated(LocalDateTime.now());
        long id = usersRepository.create(user);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/status", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStatus(@RequestBody UserStatusDto request) {
        User user = usersRepository.findById(request.getUserId());
        if (user == null) {
            throw new ResourceNotFoundException("User not found");
        }
        usersRepository.updateStatus(request.getUserId(), request.getNewStatus());
        return new ResponseEntity<>(new UserStatusDto(request.getUserId(), user.getStatus(), request.getNewStatus()), HttpStatus.OK);
    }
}
