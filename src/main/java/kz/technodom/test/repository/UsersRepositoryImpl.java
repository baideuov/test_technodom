package kz.technodom.test.repository;

import kz.technodom.test.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository("usersRepository")
public class UsersRepositoryImpl implements UsersRepository {

    private static final String SQL_INSERT = "INSERT INTO users (name, email, uri, status, created) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_STATUS = "UPDATE users\n" +
            "SET status = ?\n" +
            "WHERE id = ?";
    private static final String SQL_FIND_BY_ID = "SELECT\n" +
            "  u.id,\n" +
            "  u.name,\n" +
            "  u.email,\n" +
            "  u.status,\n" +
            "  u.uri,\n" +
            "  u.created\n" +
            "FROM users u\n" +
            "WHERE u.id = ?";
    private static final String SQL_FIND_BY_STATUS = "SELECT\n" +
            "  u.id,\n" +
            "  u.name,\n" +
            "  u.email,\n" +
            "  u.status,\n" +
            "  u.uri,\n" +
            "  u.created\n" +
            "FROM users u\n" +
            "WHERE u.status = ?";
    private static final String SQL_FIND_ALL = "SELECT\n" +
            "  u.id,\n" +
            "  u.name,\n" +
            "  u.email,\n" +
            "  u.status,\n" +
            "  u.uri,\n" +
            "  u.created\n" +
            "FROM users u";

    private JdbcOperations jdbcOperations;

    @Autowired
    public UsersRepositoryImpl(DataSource dataSource) {
        jdbcOperations = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(User user) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcOperations.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            ps.setString(1, user.getName());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getUri());
            ps.setString(4, user.getStatus() != null ? user.getStatus().toString() : null);
            ps.setTimestamp(5, Timestamp.valueOf(user.getCreated()));
            return ps;
        }, holder);
        return holder.getKey().longValue();
    }

    @Override
    public void updateStatus(long id, User.Status status) {
        jdbcOperations.update(SQL_UPDATE_STATUS, status == null ? null : status.toString(), id);
    }

    @Override
    public User findById(long id) {
        try {
            return jdbcOperations.queryForObject(SQL_FIND_BY_ID, this::mapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<User> findUsers(User.Status status) {
        try {
            if (status == null)
                return jdbcOperations.query(SQL_FIND_ALL, this::mapper);
            else
                return jdbcOperations.query(SQL_FIND_BY_STATUS, this::mapper, status.toString());
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        }
    }

    private User mapper(ResultSet rs, int index) throws SQLException {
        User user = new User();
        user.setId(rs.getLong("id"));
        user.setName(rs.getString("name"));
        user.setEmail(rs.getString("email"));
        user.setStatus(rs.getString("status") == null ? null : User.Status.valueOf(rs.getString("status")));
        user.setUri(rs.getString("uri"));
        user.setCreated(rs.getTimestamp("created").toLocalDateTime());
        return user;
    }


}
