package kz.technodom.test.repository;

import kz.technodom.test.entity.User;

import java.util.List;

public interface UsersRepository {
    long create(User user);

    void updateStatus(long id, User.Status status);

    User findById(long id);

    List<User> findUsers(User.Status status);
}
