package kz.technodom.test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {"kz.technodom.test"})
@PropertySource({
        "classpath:application.properties",
        "classpath:msg.properties",
        "classpath:msg_en.properties",
        "classpath:msg_kk.properties",
        "classpath:msg_ru.properties"})
public class RootConfig {
}
