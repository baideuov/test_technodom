package kz.technodom.test.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserStatusDto {
    @JsonProperty("user_id")
    private long userId;
    @JsonProperty("old_status")
    private User.Status oldStatus;
    @JsonProperty("new_status")
    private User.Status newStatus;

    public UserStatusDto() {
    }

    public UserStatusDto(long userId, User.Status oldStatus, User.Status newStatus) {
        this.userId = userId;
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public User.Status getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(User.Status oldStatus) {
        this.oldStatus = oldStatus;
    }

    public User.Status getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(User.Status newStatus) {
        this.newStatus = newStatus;
    }

    @Override
    public String toString() {
        return "UserStatusDto{" +
                "userId=" + userId +
                ", oldStatus=" + oldStatus +
                ", newStatus=" + newStatus +
                '}';
    }
}
