package kz.technodom.test.entity;

import java.util.List;

public class Stat {
    private final Long id;
    private final List<User> users;

    public Stat(Long id, List<User> users) {
        this.id = id;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public List<User> getUsers() {
        return users;
    }

    @Override
    public String toString() {
        return "Stat{" +
                "id=" + id +
                ", users=" + users +
                '}';
    }
}
