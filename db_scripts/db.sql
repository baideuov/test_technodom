CREATE SCHEMA `testdb` ;

CREATE TABLE `testdb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `email` VARCHAR(65) NOT NULL,
  `uri` VARCHAR(500) NULL,
  `status` VARCHAR(45) NULL,
  `created` DATETIME NOT NULL
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));